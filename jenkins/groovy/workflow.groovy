def checkoutRepo(repoUrl, path = null) {
    stage "Checkout repo"

    if (path == null) {
        sh "git clone ${repoUrl}"
    } else {
        sh "git clone ${path}"
    }
}

def commonInfo() {
    stage "Common info"
    sh 'echo $SOURCE_BRANCH'
    sh 'echo $DESTINATION_BRANCH'
    sh 'echo $PWD'
    sh 'echo $CI_USER'
    sh 'echo $GROUP'
}

def mergeBranches() {
    stage "Merge branches"
    sh 'git fetch -a'
	sh "git checkout $DESTINATION_BRANCH && git merge origin/$SOURCE_BRANCH"

    def mergeConflicts = sh (
        script: 'git ls-files -u',
        returnStdout: true
    )

    if (mergeConflicts != "") {
        println "There are some conflicts!"
        System.exit(1);
    } else {
        println "There are no conflicts!"
    }
}

def cleanup(job) {
    sh 'ansible-playbook ' +
        '-i /home/$CI_USER/ci/ansible/inventory/hosts ' +
        '/home/$CI_USER/ci/ansible/cleanup.yml ' +
        "--extra-vars='job=${job}' " +
        '--vault-password-file=/home/$CI_USER/ci/.vault_pass'
}

def runTests(application) {
    stage 'Run tests'

    switch(application) {
        case 'app':
            sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
                '/home/$CI_USER/ci/ansible/test-app.yml ' +
                '--extra-vars="project_root=$PWD" ' +
                '--connection=local ' +
                '--vault-password-file=/home/$CI_USER/ci/.vault_pass';
            break;
        case 'portal':
            sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
                '/home/$CI_USER/ci/ansible/test-portal.yml ' +
                '--extra-vars="project_root=$PWD" ' +
                '--connection=local ' +
                '--vault-password-file=/home/$CI_USER/ci/.vault_pass';
            break;
        default:
            println "Application '${application}' doesn't exists";
            break;
    }
}

def makeBuildWorkspace() {
    sh 'cp -r /home/$CI_USER/ci/build $PWD/'
    sh 'cp -r /home/$CI_USER/ci/repo $PWD/'
}

def buildImage(application, version) {
    stage 'build'
    sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
        '/home/$CI_USER/ci/ansible/build.yml ' +
        '--extra-vars="' +
            'root=$PWD ' +
            "${application}_version=${version} " +
            "application=${application}\" " +
        '--connection=local ' +
        '--vault-password-file=/home/$CI_USER/ci/.vault_pass'
}

def buildImages(portalVersion, appVersion) {
    stage 'build'
    sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
        '/home/$CI_USER/ci/ansible/build.yml ' +
        '--extra-vars="' +
            'root=$PWD ' +
            "portal_version=${portalVersion} " +
            "app_version=${appVersion}\" " +
        '--connection=local ' +
        '--vault-password-file=/home/$CI_USER/ci/.vault_pass'
}

def addVersion(application, version) {
    sh '(cd /home/$CI_USER/ci/bin/env-manager && ' +
        "./app add-app-version ${application} ${version})"
} 

def setEnvAppVersion(env, portalVersion, appVersion) {
    stage 'Set env versions'
    sh '(cd /home/$CI_USER/ci && ' +
        "./bin/change-env-app-version ${env} ${portalVersion} ${appVersion})"
}

def setEnvColor(env, color) {
    _envManagerCmd("set-env-color ${env} ${color}")
}

def _getShResult(cmd) {
    return sh (
        script: cmd,
        returnStdout: true
    ).trim()
}

def _envManagerCmd(cmd) {
    return '(cd /home/$CI_USER/ci/bin/env-manager && ./app ' + cmd + ')'
}

def deploy(env, destroyOldColor) {
    stage 'Deploy'

    def newColor = _getShResult(_envManagerCmd("get-env-color ${env}"))
    def oldColor = newColor == 'blue' ? 'green' : 'blue'

    def portalVersion = _getShResult(_envManagerCmd("get-env-app-version ${env} ${newColor} portal"))
    def appVersion = _getShResult(_envManagerCmd("get-env-app-version ${env} ${newColor} app"))
    
    def host = _getShResult(_envManagerCmd("get-env-host ${env} ${newColor}"))
    def port = _getShResult(_envManagerCmd("get-env-port ${env} ${newColor}"))

    try {
        sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
            '/home/$CI_USER/ci/ansible/deploy.yml ' +
            '--extra-vars="' +
                'root=$PWD ' +
                "env=${env} " +
                "host=${host} " +
                "host_port=${port} " +
                "destroy_old_color=${destroyOldColor} " +
                "new_color=${newColor} " +
                "old_color=${oldColor} " +
                "portal_version=${portalVersion} " +
                "app_version=${appVersion}" +
            '" ' +
            '--vault-password-file=/home/$CI_USER/ci/.vault_pass'
    } catch(e) {
        println(e.toString());
        println(e.getMessage());
        println(e.getStackTrace()); 
        rollback(env)
    }
} 

def rollback(env) {
    def newColor = _getShResult(_envManagerCmd("get-env-color ${env}"))
    def oldColor = newColor == 'blue' ? 'green' : 'blue'

    def host = _getShResult(_envManagerCmd("get-env-host ${env} ${oldColor}"))
    def port = _getShResult(_envManagerCmd("get-env-port ${env} ${oldColor}"))

    sh 'ansible-playbook -i /home/$CI_USER/ci/ansible/inventory/hosts ' +
        '/home/$CI_USER/ci/ansible/host-proxy-port.yml ' +
        '--extra-vars="' +
            'root=$PWD ' +
            "env=${env} " +
            "host=${host} " +
            "host_port=${port} " +
            "new_color=${oldColor}" +
        '" ' +
        '--vault-password-file=/home/$CI_USER/ci/.vault_pass'

    setEnvColor(env, oldColor)
}

def getEnvByBranch(branch) {
    switch (branch) {
        case 'test-develop':
            return 'dev';
        case 'test-staging':
            return 'stage';
        default: return '';
    }
}

def getEnv(envName) {
    return sh (
        script: "echo \$${envName}",
        returnStdout: true
    ).trim()
}

def getEnvColor(env) {
    return _getShResult(_envManagerCmd("get-env-color ${env}"))
}

def getEnvAppVersion(env, application) {
    def currentColor = _getShResult(_envManagerCmd("get-env-color ${env}"))

    return _getShResult(_envManagerCmd("get-env-app-version ${env} ${currentColor} ${application}"))
}

def slack(message, to = '', icon = '') {

} 

def sendSlackMessage(message, icon = ':pepecool:') {
    sh 'curl -X POST ' +
        '--data-urlencode "payload={' +
            '\\\"channel\\\": \\\"#jenkins\\\", ' +
            '\\\"username\\\": \\\"jenkins CI\\\", ' +
            '\\\"text\\\": \\\"' + message + '\\\", ' +
            '\\\"icon_emoji\\\": \\\"' + icon + '\\\"}" ' +
            'https://hooks.slack.com/services/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'
}

def getSlackUsernameByGitUsername(gitUsername) {
    switch(gitUsername) {
        case 'olegshdev':
            return 'oleg.s'
        case 'eugene-m':
            return 'eugene'
        case 'alexeydibrovalight':
            return 'alexey_di'
        default:
            return ''
    }
}

return this;
