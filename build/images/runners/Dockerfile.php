FROM richarvey/nginx-php-fpm:1.5.3

RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories
RUN apk --no-cache add shadow

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN groupmod -g $GROUP_ID nginx && usermod -u $USER_ID nginx

RUN apk update && apk add --no-cache \
    mysql-client \
    libxml2-dev \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install xml

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
RUN mkdir /.composer && chmod -R 777 /.composer

USER nginx

RUN composer global require hirak/prestissimo

WORKDIR /var/www/app

